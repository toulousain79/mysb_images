#  __/\\\\____________/\\\\___________________/\\\\\\\\\\\____/\\\\\\\\\\\\\___
#   _\/\\\\\\________/\\\\\\_________________/\\\/////////\\\_\/\\\/////////\\\_
#	_\/\\\//\\\____/\\\//\\\____/\\\__/\\\__\//\\\______\///__\/\\\_______\/\\\_
#	 _\/\\\\///\\\/\\\/_\/\\\___\//\\\/\\\____\////\\\_________\/\\\\\\\\\\\\\\__
#	  _\/\\\__\///\\\/___\/\\\____\//\\\\\________\////\\\______\/\\\/////////\\\_
#	   _\/\\\____\///_____\/\\\_____\//\\\____________\////\\\___\/\\\_______\/\\\_
#		_\/\\\_____________\/\\\__/\\_/\\\______/\\\______\//\\\__\/\\\_______\/\\\_
#		 _\/\\\_____________\/\\\_\//\\\\/______\///\\\\\\\\\\\/___\/\\\\\\\\\\\\\/__
#		  _\///______________\///___\////__________\///////////_____\/////////////_____
#			By toulousain79 ---> https://github.com/toulousain79/
#
######################################################################
#
#	Copyright (c) 2013 toulousain79 (https://github.com/toulousain79/)
#	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#	--> Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
#
##################### FIRST LINE #####################################
# https://hub.docker.com/r/_/alpine/
# https://github.com/just-containers/s6-overlay#usage
######################################################################
#### Arguments (outside of a build stage)
ARG REGISTRY_IMAGE

##### Base image
FROM ${REGISTRY_IMAGE}/base-alpine/base-alpine:dev

#### Arguments (inside of a build stage)
ARG PROJECT_NAMESPACE
ARG PROJECT_NAME
ARG BUILD_DATE
ARG BUILD_VERSION
ARG BUILD_REF
ARG PROJECT_NAME
ARG PROJECT_DESCRIPTION
ARG PROJECT_URL

#### Environment variables
# Use in multi-phase builds, when an init process requests for the container to gracefully exit, so that it may be committed
# Used with alternative CMD (worker.sh), leverages supervisor to maintain long-running processes
ENV PROJECT_NAMESPACE=${PROJECT_NAMESPACE} \
    PROJECT_NAME=${PROJECT_NAME} \
    BUILD_DATE=${BUILD_DATE} \
    BUILD_VERSION=${BUILD_VERSION} \
    BUILD_REF=${BUILD_REF} \
    PROJECT_DESCRIPTION=${PROJECT_DESCRIPTION} \
    PROJECT_URL=${PROJECT_URL} \
    S6_KILL_GRACETIME=3000 \
    S6_KILL_FINISH_MAXTIME=5000 \
    S6_BEHAVIOUR_IF_STAGE2_FAILS=2 \
    SIGNAL_BUILD_STOP=99

ENV LIBTORRENT_VER=v0.13.8 \
    RTORRENT_VER=v0.9.8 \
    MEDIAINFO_VER=19.09 \
    LIBZEN_VER=0.4.31 \
    WITH_FILEBOT=YES \
    FILEBOT_VER=4.7.9 \
    CHROMAPRINT_VER=1.4.3 \
    GEOIP_VER=1.1.1

ENV UID=991 \
    GID=991 \
    WEBROOT=/ \
    PORT_RTORRENT=45000 \
    DHT_RTORRENT=off \
    DISABLE_PERM_DATA=false \
    PKG_CONFIG_PATH=/usr/local/lib/pkgconfig \
    BIN_CURL='curl --retry 3 --silent --insecure --location --show-error'

#### Metadata
LABEL maintainer="${PROJECT_NAMESPACE}" \
    label.build-date="${BUILD_DATE}" \
    label.build-version="${BUILD_VERSION}" \
    label.build-ref="${BUILD_REF}" \
    label.name="${PROJECT_NAME} - rTorrent/ruTorrent" \
    label.description="${PROJECT_DESCRIPTION}" \
    label.url="${PROJECT_URL}" \
    label.vendor="MySB - ${PROJECT_NAMESPACE}" \
    label.usage="${PROJECT_URL}/blob/${BUILD_VERSION}/README.md" \
    label.changelog-url="${PROJECT_URL}/blob/${BUILD_VERSION}/Changelog.md"

#### Copy files
COPY ./rootfs /tmp/rootfs

#### First
RUN \
    cp -rv /tmp/rootfs/* / \
    && chmod +x /usr/local/bin/* \
    && /bin/ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
    && /bin/ln -fs /usr/share/zoneinfo/${TZ} /etc/timezone \
    && docker_clean

#### Debian Packages & Python packages
# PKG_BASE: Packages needed for your image to add to 'rootfs/install/pkg_base.txt'.
# PKG_TEMP: Packages needed only for the build and can be remove to add to 'rootfs/install/pkg_temp.txt'.
# PKG_TO_DEL: Package alraedy installed but not needed and to remove to add to 'rootfs/install/pkg_to_del.txt'.
# PIP_BASE: Python 2 packages to add to 'rootfs/install/requirements2.txt'.
# PIP3_BASE: Python 3 packages to add to 'rootfs/install/requirements3.txt'.
# NOTE: You must add python-pip or python3-pip into PKG_TEMP if you want use PIP or PIP3 !!!
# NOTE: Leave following with one layer !!!
RUN \
    # Debian packages
    aptinstaller \
    # Python
    && pipinstaller \
    # Cleaning
    && docker_clean

#### Compile
RUN \
    # mktorrent
    git clone https://github.com/esmil/mktorrent /tmp/mktorrent \
    && cd /tmp/mktorrent \
    && make -j ${BUILD_CORES-$(grep -c "processor" /proc/cpuinfo)} \
    && make install \
    && strip -s /usr/local/bin/mktorrent \
    && docker_clean

# XMLRPC-C
RUN git clone https://github.com/mirror/xmlrpc-c.git /tmp/xmlrpc-c \
    && cd /tmp/xmlrpc-c/stable \
    && ./configure --disable-abyss-server --disable-abyss-threads --disable-abyss-openssl --disable-cgi-server --disable-cplusplus --disable-libwww-client --disable-wininet-client \
    && make -j ${NB_CORES} \
    && make install \
    && docker_clean

# ZenLib
RUN ${BIN_CURL} http://downloads.sourceforge.net/zenlib/libzen_${LIBZEN_VER}.tar.gz -o /tmp/libzen_${LIBZEN_VER}.tar.gz \
    && cd /tmp \
    && tar xzf libzen_${LIBZEN_VER}.tar.gz \
    && cd /tmp/ZenLib/Project/GNU/Library \
    && ./autogen \
    && ./configure --prefix=/usr/local --enable-shared --disable-static \
    && make && make install \
    && docker_clean

# Mediainfo DLL & CLI
RUN ${BIN_CURL} http://mediaarea.net/download/binary/libmediainfo0/${MEDIAINFO_VER}/MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz -o /tmp/MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz \
    && cd /tmp \
    && tar xzf MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz \
    && cd /tmp/MediaInfo_DLL_GNU_FromSource \
    && ./SO_Compile.sh \
    && cd /tmp/MediaInfo_DLL_GNU_FromSource/ZenLib/Project/GNU/Library \
    && make install \
    && cd /tmp/MediaInfo_DLL_GNU_FromSource/MediaInfoLib/Project/GNU/Library \
    && make install \
    && ${BIN_CURL} http://mediaarea.net/download/binary/mediainfo/${MEDIAINFO_VER}/MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz -o /tmp/MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz \
    && cd /tmp \
    && tar xzf MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz \
    && cd /tmp/MediaInfo_CLI_GNU_FromSource \
    && ./CLI_Compile.sh \
    && cd /tmp/MediaInfo_CLI_GNU_FromSource/MediaInfo/Project/GNU/CLI \
    && make install \
    && strip -s /usr/local/bin/mediainfo \
    && docker_clean

# libTorrrent & rTorrent
RUN git clone -b ${LIBTORRENT_VER} https://github.com/rakshasa/libtorrent.git /tmp/libtorrent \
    && cd /tmp/libtorrent \
    && ./autogen.sh \
    && ./configure --disable-debug --disable-instrumentation \
    && make -j ${BUILD_CORES-$(grep -c "processor" /proc/cpuinfo)} \
    && make install \
    && git clone -b ${RTORRENT_VER} https://github.com/rakshasa/rtorrent.git /tmp/rtorrent \
    && cd /tmp/rtorrent \
    && ./autogen.sh \
    && ./configure --enable-ipv6 --disable-debug --with-xmlrpc-c \
    && make -j ${BUILD_CORES-$(grep -c "processor" /proc/cpuinfo)} \
    && make install \
    && strip -s /usr/local/bin/rtorrent \
    && docker_clean

# Plowshare
RUN git clone https://github.com/mcrapet/plowshare /tmp/plowshare \
    && cd /tmp/plowshare \
    && make \
    && docker_clean

# ruTorrent
COPY ./packages /tmp
RUN git clone https://github.com/Novik/ruTorrent.git /var/www/html/rutorrent \
    # Hack MySB
    && mv /tmp/rtorrent.php.tmpl /var/www/html/rutorrent/php/rtorrent.php \
    # filemanager / fileshare / fileupload / mediastream
    && git clone https://github.com/nelu/rutorrent-thirdparty-plugins /tmp/rutorrent-thirdparty-plugins \
    && mv -v /tmp/rutorrent-thirdparty-plugins/* /var/www/html/rutorrent/plugins/ \
    # Mediastream
    # && unzip -qv /tmp/mediastream_v0.01.zip -d /var/www/html/rutorrent/plugins/ \
    # Mobile
    && git clone https://github.com/xombiemp/rutorrentMobile.git /var/www/html/rutorrent/plugins/mobile \
    # For Rsync
    && git clone https://github.com/InAnimaTe/for_rsync.git /var/www/html/rutorrent/plugins/for_rsync \
    # Theme MaterialDesign
    && git clone https://github.com/Phlooo/ruTorrent-MaterialDesign.git /var/www/html/rutorrent/plugins/theme/themes/materialdesign \
    # Theme club-QuickBox
    && git clone https://github.com/QuickBox/club-QuickBox.git /var/www/html/rutorrent/plugins/theme/themes/club-QuickBox \
    # GeoIP 2
    && git clone https://github.com/Micdu70/geoip2-rutorrent /var/www/html/rutorrent/plugins/geoip2 \
    # Ratiocolor
    && git clone https://github.com/Gyran/rutorrent-ratiocolor.git /var/www/html/rutorrent/plugins/ratiocolor \
    && perl -pi -e 's/changeWhat = "cell-background";/changeWhat = "font";/g' /var/www/html/rutorrent/plugins/ratiocolor/init.js \
    # Chat
    && tar zxvf /tmp/chat_v2.0.tar.gz -C /var/www/html/rutorrent/plugins/ \
    # Check SFV
    && unzip /tmp/checksfv_v3.6.zip -d /var/www/html/rutorrent/plugins/ \
    && perl -pi -e "s/'';/'\/usr\/bin\/cksfv';/g" /var/www/html/rutorrent/plugins/checksfv/conf.php \
    && cp -fv /var/www/html/rutorrent/plugins/checksfv/lang/en.js /var/www/html/rutorrent/plugins/checksfv/lang/fr.js \
    && perl -pi -e "s/Check SFV/V&eacute;rification SFV/g" /var/www/html/rutorrent/plugins/checksfv/lang/fr.js \
    # Instant search
    && unzip /tmp/instantsearch_v1.0.zip -d /var/www/html/rutorrent/plugins/ \
    # LBLL Suite
    && tar zxvf /tmp/lbll-suite_v0.8.1.tar.gz -C /var/www/html/rutorrent/plugins/ \
    # Link Cakebox
    && unzip /tmp/linkcakebox_v1.0.zip -d /var/www/html/rutorrent/plugins/ \
    # Link Seedbox-Manager
    && unzip /tmp/linkseedboxmanager_v1.0.zip -d /var/www/html/rutorrent/plugins/ \
    # Logoff
    && tar zxvf /tmp/logoff_v1.3.tar.gz -C /var/www/html/rutorrent/plugins/ \
    # NFO
    && unzip /tmp/nfo_v3.6.zip -d /var/www/html/rutorrent/plugins/ \
    && cp -fv /var/www/html/rutorrent/plugins/nfo/lang/en.js /var/www/html/rutorrent/plugins/nfo/lang/fr.js \
    && perl -pi -e "s/NFO Viewer/Visionneuse NFO/g" /var/www/html/rutorrent/plugins/nfo/lang/fr.js \
    && perl -pi -e "s/Could not open NFO/Impossible d'ouvrir le NFO/g" /var/www/html/rutorrent/plugins/nfo/lang/fr.js \
    && perl -pi -e "s/ERROR/ERREUR/g" /var/www/html/rutorrent/plugins/nfo/lang/fr.js \
    # Pause WebUI
    && unzip /tmp/pausewebui_v1.2.zip -d /var/www/html/rutorrent/plugins/ \
    # Show IP
    && unzip /tmp/showip_v3.6.zip -d /var/www/html/rutorrent/plugins/ \
    # SpeedGraph
    && unzip /tmp/speedgraph_v3.6.zip -d /var/www/html/rutorrent/plugins/ \
    && cp -fv /var/www/html/rutorrent/plugins/speedgraph/lang/en.js /var/www/html/rutorrent/plugins/speedgraph/lang/fr.js \
    && perl -pi -e "s/Monitoring Time/Surveillance du temps/g" /var/www/html/rutorrent/plugins/speedgraph/lang/fr.js \
    && perl -pi -e "s/Speed Graph/Graphique de vitesse/g" /var/www/html/rutorrent/plugins/speedgraph/lang/fr.js \
    # Stream
    && tar zxvf /tmp/stream_v1.0.tar.gz -C /var/www/html/rutorrent/plugins/ \
    && perl -pi -e "s/define\('USE_NGINX', false\);/define\('USE_NGINX', true\);/g" /var/www/html/rutorrent/plugins/stream/config.php \
    && perl -pi -e "s/define\('SCHEME', 'http'\);/define\('SCHEME', 'https'\);/g" /var/www/html/rutorrent/plugins/stream/config.php \
    # Sync
    && unzip /tmp/sync_v0.1.zip -d /var/www/html/rutorrent/plugins/ \
    # Theme Oblivion
    && echo "/* for Oblivion */" | tee -a /var/www/html/rutorrent/css/style.css \
    && echo ".meter-value-start-color { background-color: #E05400 }" | tee -a /var/www/html/rutorrent/css/style.css \
    && echo ".meter-value-end-color { background-color: #8FBC00 }" | tee -a /var/www/html/rutorrent/css/style.css \
    && echo "::-webkit-scrollbar {width:12px;height:12px;padding:0px;margin:0px;}" | tee -a /var/www/html/rutorrent/css/style.css \
    && perl -pi -e "s/\$defaultTheme \= \"\"\;/\$defaultTheme \= \"Oblivion\"\;/g" /var/www/html/rutorrent/plugins/theme/conf.php \
    # GeoIP
    && cd /usr/share/GeoIP \
    && ${BIN_CURL} https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz -o ./GeoLite2-City.tar.gz \
    && ${BIN_CURL} https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz -o ./GeoLite2-Country.tar.gz \
    && tar xzf GeoLite2-City.tar.gz \
    && tar xzf GeoLite2-Country.tar.gz \
    && rm -f *.tar.gz \
    && mv GeoLite2-*/*.mmdb . \
    && cp *.mmdb /var/www/html/rutorrent/plugins/geoip2/database/ \
    && pecl install geoip-${GEOIP_VER} \
    && chmod +x /usr/lib/php7/modules/geoip.so \
    ## cleanup
    # Remove unwanted files
    && rm -vf /var/www/html/rutorrent/htaccess-example \
    && rm -vf /var/www/html/rutorrent/plugins/*.md \
    # && rm -vf /var/www/html/rutorrent/plugins/extsearch/engines/NNMClub.php \
    && docker_clean

#### Outside
# perl -pi -e "s/http:\/\/mydomain.com\/share.php/https:\/\/${gsHostNameFqdn}:${gsPort_MySB_HTTPS}\/fileshare.php/g" /var/www/html/rutorrent/plugins/fileshare/conf.php
# perl -pi -e "s/http:\/\/mydomain.com\/stream\/view.php/https:\/\/${gsHostNameFqdn}:${gsPort_MySB_HTTPS}\/view/g" /var/www/html/rutorrent/plugins/mediastream/conf.php

#### Filebot
ENV filebot_version="${FILEBOT_VER}" \
    chromaprint_ver="${CHROMAPRINT_VER}" \
    FILEBOT_RENAME_METHOD="symlink" \
    FILEBOT_RENAME_MOVIES="{n} ({y})" \
    FILEBOT_RENAME_SERIES="{n}/Season {s.pad(2)}/{s00e00} - {t}" \
    FILEBOT_RENAME_ANIMES="{n}/{e.pad(3)} - {t}" \
    FILEBOT_RENAME_MUSICS="{n}/{fn}" \
    FILEBOT_LANG="fr" \
    FILEBOT_CONFLICT=skip

RUN if [ "${WITH_FILEBOT}" == "YES" ]; then \
    mkdir /filebot \
    && cd /filebot \
    && ${BIN_CURL} http://downloads.sourceforge.net/project/filebot/filebot/FileBot_${FILEBOT_VER}/FileBot_${FILEBOT_VER}-portable.tar.xz -o ./FileBot_${FILEBOT_VER}-portable.tar.xz \
    && tar xJf FileBot_${FILEBOT_VER}-portable.tar.xz \
    && ln -sf /usr/local/lib/libzen.so.0.0.0 /filebot/lib/x86_64/libzen.so \
    && ln -sf /usr/local/lib/libmediainfo.so.0.0.0 /filebot/lib/x86_64/libmediainfo.so \
    && ${BIN_CURL} https://github.com/acoustid/chromaprint/releases/download/v${CHROMAPRINT_VER}/chromaprint-fpcalc-${CHROMAPRINT_VER}-linux-x86_64.tar.gz -o ./chromaprint-fpcalc-${CHROMAPRINT_VER}-linux-x86_64.tar.gz \
    && tar xvf chromaprint-fpcalc-${CHROMAPRINT_VER}-linux-x86_64.tar.gz \
    && mv chromaprint-fpcalc-${CHROMAPRINT_VER}-linux-x86_64/fpcalc /usr/local/bin \
    && strip -s /usr/local/bin/fpcalc \
    && rm -rf /filebot/FileBot_${FILEBOT_VER}-portable.tar.xz \
    /filebot/chromaprint-fpcalc-${CHROMAPRINT_VER}-linux-x86_64.tar.gz\
    /filebot/chromaprint-fpcalc-${CHROMAPRINT_VER}-linux-x86_64 \
    && docker_clean \
    ;fi

#### Network ports
EXPOSE 51101/tcp 51101/udp 51102/tcp

#### Volumes
VOLUME ["/rtorrent/complete", "/rtorrent/watch", "/rtorrent/torrents", "/rtorrent/.session", "/rtorrent/config.d", "/rtorrent/scripts", "/rtorrent/config", "/rtorrent/logs"]

#### Cleaning
RUN docker_clean 'END'

#### END
ENTRYPOINT ["/init"]


# VOLUME /data /config
# EXPOSE 8080
# RUN chmod +x /usr/local/bin/startup

# ENTRYPOINT ["/usr/local/bin/startup"]
# CMD ["/bin/s6-svscan", "/etc/s6.d"]
