# MySB_images

<!-- <p align="center">
  <a href="https://img.shields.io/gitlab/pipeline/toulousain79/MySB_images/master.svg?label=develop%20pipeline%20status&style=flat-square">
    <img alt="master pipeline status" src="https://img.shields.io/gitlab/pipeline/toulousain79/MySB_images/master.svg?label=develop%20pipeline%20status&style=flat-square" /></a>
  <a href="https://img.shields.io/gitlab/pipeline/toulousain79/MySB_images/develop.svg?label=develop%20pipeline%20status&style=flat-square">
    <img alt="develop pipeline status" src="https://img.shields.io/gitlab/pipeline/toulousain79/MySB_images/develop.svg?label=develop%20pipeline%20status&style=flat-square" /></a>
</p> -->

<p align="center">
    <a href="https://gitlab.com/toulousain79/MySB_images/blob/master/LICENCE.md"><img src="https://img.shields.io/github/license/mashape/apistatus.svg?style=flat-square" /></a>
</p>

## Images

### Base Alpine

### Base Debian Buster

### Project Check

### rTorrent

## Documentations

- [s6-overlay](doc/s6-overlay.md)

## Links

- [s6-overlay](https://github.com/just-containers/s6-overlay)
- [alpinelinux](https://www.alpinelinux.org/)
- [debian-slim](https://hub.docker.com/_/debian/)
- [godnsmasq](https://github.com/janeczku/go-dnsmasq)
- [factorish](https://github.com/factorish/factorish)

## TODO
